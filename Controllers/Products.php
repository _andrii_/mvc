<?php

namespace MVC\Controllers;

use MVC\Models\Category;
use MVC\Models\Product;

class Products extends Controller
{
    public function index(array $request = [])
    {
        $data = [];
        if (!empty($request['cat_id'])) {
            $data['products'] = Product::getByField('category_id', (int)$request['cat_id']);
        } else {
            $data['products'] = Product::getAll();
        }
        $data['categories'] = Category::getAll();
        $this->view->render('shop', $data);
    }

    protected function before()
    {
        // TODO: Implement before() method.
        if (empty($_SESSION['user'])) {
            header('Location: /user/login');
            die();
        }
    }
}