<?php

namespace MVC\Controllers;

use MVC\Models\Cart as NewCart;
use MVC\Models\CartProduct;

class Cart extends Controller
{
    public function createcart(array $request = [])
    {
        $data = [];
        if (!empty($request['products'])) {
            if (empty($_SESSION['cart_id'])) {
                $data['cartId'] = NewCart::createCart($_SESSION['user']->id);
                if (!empty($data['cartId'])) {
                    $_SESSION['cart_id'] = $data['cartId'];
                }
            }
            CartProduct::addProductsToCart($_SESSION['cart_id']->getId(), $request['products']);
        }
        if (!empty($_SESSION['cart_id'])) {
        $data['selectedProducts'] = CartProduct::getProductsFromCart($_SESSION['cart_id']->getId());
        }
        $this->view->render('cart', $data);
    }

    protected function before()
    {
        // TODO: Implement before() method.
        if (empty($_SESSION['user'])) {
            header('Location: /user/login');
            die();
        }
    }
}