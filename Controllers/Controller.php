<?php

namespace MVC\Controllers;

use MVC\Views\View;

abstract class Controller
{
    protected View $view;

    public function __construct()
    {
        $this->before();
        $this->view = new View();
    }

    abstract protected function before();
}