<?php

namespace MVC\Controllers;

use MVC\Models\User as UserModel;

class User extends Controller
{
    public function login(array $request = [])
    {
        $data = [];
        if (!empty($request['email']) && !empty($request['password'])) {
            $user = UserModel::login($request['email'], $request['password']);
            $data['errors'] = 0;
            if (!empty($user)) {
                $_SESSION['user'] = $user;
                header('Location: /');
                die();
            } else {
                $data['errors'] = "Login credentials are invalid";
            }
        }
        $this->view->render('Login', $data);
    }

    public function registration(array $request = [])
    {
        $fields = ["name", "email", "password", "password_confirm"];
        $data = [];
        if (!empty($request)) {
            $data['errors'] = [];
            foreach ($fields as $field) {
                if (empty($request[$field])) {
                    $data['errors'][] = "Field " . ucwords(str_replace("_", " ", $field)) . " is required";
                }
            }

            if (!empty($request['email']) && UserModel::checkIfUnique($request['email'], 'users', 'email')) {
                $data['errors'][] = "Email must be unique";
            }

            if ($request['password'] !== $request['password_confirm']) {
                $data['errors'][] = "Password must be confirmed";
            }

            if (empty($data['errors'])) {
                $user = UserModel::save($request['name'], $request['email'], $request['password']);
                $_SESSION['user'] = UserModel::getById($user->id);
                header('Location: /');
                die();
            }
        }
        $this->view->render('registration', $data);
    }

    public function logout()
    {
        die(1111111);
    }

    protected function before()
    {
        if (!empty($_SESSION['user'])) {
            header('Location: /');
            die();
        }
    }
}