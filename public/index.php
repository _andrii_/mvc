<?php

define('ROOT_PATH', dirname(__FILE__, 2));

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use MVC\Routes\WebRouter;

session_start();

$dotenv = Dotenv\Dotenv::createImmutable(ROOT_PATH);
$dotenv->load();

$router = new WebRouter([
    "/" => ["controller" => "products", "action" => "index"],
    "user/login" => ["controller" => "user", "action" => "login"],
    "user/registration" => ["controller" => "user", "action" => "registration"],
    "cart/createcart" => ["controller" => "cart", "action" => "createcart"],
    "user/logout" => ["controller" => "user", "action" => "logout"]
    ]);

debug($_REQUEST);

$_REQUEST['path'] = empty($_REQUEST['path']) ? "/" : $_REQUEST['path'];
$router->run($_REQUEST);


function debug($val)
{
    echo '<pre>' . print_r($val, 1) . '</pre>';
}

