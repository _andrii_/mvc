<?php
namespace MVC\Models;

class Product
{
    protected string $id;
    protected string $category_id;
    protected string $title;
    protected string $description;
    protected float $price;
    protected int $qty;


    public function getId(): string
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCategoryId(): string
    {
        return $this->category_id;
    }

    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getQty(): int
    {
        return $this->qty;
    }

    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    public static function getAll(): array
    {
        $smtp = Db::getInstance()->pdo->prepare("SELECT * FROM `products`");
        $smtp->execute([]);
        $productsArray = $smtp->fetchAll();
        $products = [];
        foreach ($productsArray as $prod) {
            $product = new Product();
            $product->id = $prod['id'];
            $product->category_id = $prod['category_id'];
            $product->title = $prod['title'];
            $product->description = $prod['description'];
            $product->price = $prod['price'];
            $product->qty = $prod['qty'];
            $products[] = $product;
        }
        return $products;
    }
    public static function getByField(string $field, string $value): array
    {
        $smtp = Db::getInstance()->pdo->prepare("
            SELECT
                `products`.*
            FROM
                `products`
            WHERE
                " . $field . " = :value
        ");
        $smtp->execute(["value" => $value]);
        $productsArray =  $smtp->fetchAll();
        $products = [];
        foreach ($productsArray as $prod) {
            $product = new Product();
            $product->id = $prod['id'];
            $product->category_id = $prod['category_id'];
            $product->title = $prod['title'];
            $product->description = $prod['description'];
            $product->price = $prod['price'];
            $product->qty = $prod['qty'];
            $products[] = $product;
        }
        return $products;
    }

}