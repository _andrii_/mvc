<?php
namespace MVC\Models;


use MVC\Models\Interfaces\CheckUnique;

class User extends AbstractUser implements CheckUnique
{
    public function __construct(
        string $name,
        string $email,
        string|null $password = null,
        string|null $id = null,
        string $status = 'Active'
    ) {
        if (!empty($id)) {
            $this->id = $id;
        }
        $this->setName($name);
        if (empty($id)) {
            $this->setEmail($email);
            if (!empty($password)) {
                $this->setPassword($password);
            }
        } else {
            $this->email = $email;
            $this->password = $password;
        }
        $this->status = $status;
    }

    public function setName(string $name)
    {
        if ($this->checkName($name)) {
            $this->name = $name;
        } else {
            throw new \Exception('Name is invalid');
        }
    }

    public function setEmail(string $email)
    {
        if (self::checkIfUnique($email, "users", "email")) {
            throw new \Exception('Field email must be unique');
        }
        $this->email = $email;
    }

    private function checkName(string $name): bool
    {
        if (strlen($name) >= 3 && strlen($name) < 150) {
            return true;
        }
        return false;
    }

    protected function crypt(string $text)
    {
        return password_hash($text, PASSWORD_DEFAULT);
    }

    public static function checkIfUnique(string $value, string $table, string $column): bool
    {
        // TODO: Implement checkIfUnique() method.
        $stmt = Db::getInstance()->pdo->prepare(
            "
            SELECT
                `id`
            FROM
                " . $table . "
            WHERE
                " . $column . " = :value
        "
        );
        $stmt->execute(["value" => $value]);
        return (bool)$stmt->fetch();
    }

    public static function save(string $name, string $email, string $password): User
    {
        $user = new User($name, $email, $password);
        $stmt = DB::getInstance()->pdo->prepare(
            "
            INSERT INTO `users` (
                `name`,
                `email`,
                `password`,
                `status`
            )
            VALUES
                (
                    :name,
                    :email,
                    :password,
                    :status
                )"
        );
        $stmt->execute(
            [
                "name" => $user->getName(),
                "email" => $user->getEmail(),
                "password" => $user->getPassword(),
                "status" => 'Active'
            ]
        );
        $user->id = Db::getInstance()->pdo->lastInsertId();
        return $user;
    }

    public static function getById(int $userId): User|null
    {
        if (empty($userId)) {
            return null;
        }
        $stmt = Db::getInstance()->pdo->prepare("SELECT * FROM `users` WHERE `id` = :id");
        $stmt->execute(["id" => $userId]);
        $userArray = $stmt->fetch();
        if (empty($userArray)) {
            return null;
        }
        $user = new User(
            $userArray['name'],
            $userArray['email'],
            $userArray['password'],
            $userArray['id'],
            $userArray['status']
        );
        $user->setCreateAt($userArray['create_at']);
        return $user;
    }

    public static function login(string $email, string $password): User|null
    {
        $stmt = Db::getInstance()->pdo->prepare(
            "
                SELECT
                    `users`.*
                FROM
                    `users`
                WHERE
                    `email` = :email
            "
        );
        $stmt->execute(["email" => $email]);
        $userArray = $stmt->fetch();
        if (password_verify($password, $userArray['password'])) {
            $user = new User(
                $userArray['name'],
                $userArray['email'],
                $userArray['password'],
                $userArray['id'],
                $userArray['status']
            );
            $user->setCreateAt($userArray['create_at']);
            return $user;
        }
        return null;
    }

    public static function logoutUser()
    {
        unset($_SESSION['user']);
        header('Location: /');
    }
}
