<?php
namespace MVC\Models;

abstract class AbstractUser
{
    public string|null $id = null;
    protected string $name;
    protected string $email;
    protected string $password;
    protected string $status;
    protected string $create_at;

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getCreateAt()
    {
        return $this->create_at;
    }

    public function setCreateAt(string $createAt)
    {
        $this->create_at = $createAt;
    }

    public function setPassword(string $password)
    {
        return $this->password = $this->crypt($password);
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    protected abstract function crypt(string $text);
}
