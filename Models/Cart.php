<?php
namespace MVC\Models;

class Cart
{
    protected string $id;
    protected string $user_id;
    protected string $ordered_at;


    public function __construct(string $user_id)
    {
        $this->user_id = $user_id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getUserId(): string
    {
        return $this->user_id;
    }

    public function setUserId(string $user_id): void
    {
        $this->user_id = $user_id;
    }

    public function getOrderedAt(): string
    {
        return $this->ordered_at;
    }

    public function setOrderedAt(string $ordered_at): void
    {
        $this->ordered_at = $ordered_at;
    }

    public static function createCart(string $userId): Cart
    {
        $cart = new Cart($userId);
        $stmt = Db::getInstance()->pdo->prepare(
            "INSERT INTO `cart`(`user_id`) 
         VALUES( :user_id )"
        );
        $stmt->execute(["user_id" => $cart->getUserId()]);
        $cart->id = Db::getInstance()->pdo->lastInsertId();
        return $cart;
    }

}