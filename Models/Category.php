<?php
namespace MVC\Models;

class Category
{
    protected string $id;
    protected string $name;


    public function getId(): string
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public static function getAll(): array
    {
        $stmt = Db::getInstance()->pdo->prepare("SELECT * FROM `categories`");
        $stmt->execute([]);
        $categoriesArray = $stmt->fetchAll();
        if (empty($categoriesArray)) {
            return [];
        }
        $categories = [];
        foreach ($categoriesArray as $category) {
            $cat = new Category();
            $cat->setId($category['id']);
            $cat->setName($category['name']);
            $categories[] = $cat;
        }
        return $categories;
    }
}