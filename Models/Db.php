<?php
namespace MVC\Models;

class Db
{
    private static $instance = null;
    public object $pdo;


    protected function __construct()
    {
    }

    public static function getInstance(): Db
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
            $dsn = "mysql:host=localhost;port=3306;dbname=" . $_ENV["DB_NAME"] . ";charset=utf8";
            $options = [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
            ];

            self::$instance->pdo = new \PDO($dsn, $_ENV["DB_USER"], $_ENV["DB_PASS"], $options);
        }
        return self::$instance;
    }


}