<?php
namespace MVC\Models;

//use MVC\Models\Product;

class CartProduct extends Product
{
    protected string $cart_id;
    protected string $product_id;
    protected string $selected_quantity;

    /**
     * @return int
     */
    public function getCartId(): string
    {
        return $this->cart_id;
    }

    /**
     * @param string $cart_id
     */
    public function setCartId(string $cart_id): void
    {
        $this->cart_id = $cart_id;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->product_id;
    }

    /**
     * @param string $product_id
     */
    public function setProductId(string $product_id): void
    {
        $this->product_id = $product_id;
    }

    /**
     * @return string
     */
    public function getSelectedQuantity(): string
    {
        return $this->selected_quantity;
    }

    /**
     * @param string $selected_quantity
     */
    public function setSelectedQuantity(string $selected_quantity): void
    {
        $this->selected_quantity = $selected_quantity;
    }

    public static function addProductsToCart(string $cartId, array $products): bool
    {
        if (empty($cartId) || empty($products)) {
            return false;
        }
        foreach ($products as $product) {
            $isProductAdded = CartProduct::getProductFromCart($cartId, $product);
            if (!empty($isProductAdded)) {
                //update product quantity
            } else {
                $smtp = Db::getInstance()->pdo->prepare(
                    "
                INSERT INTO `cart_products` (
                    `cart_id`,
                    `product_id`,
                    `quantity`
                )
                VALUES
                (
                    :cart_id,
                    :product_id,
                    :quantity
                )"
                );
                $smtp->execute(
                    [
                        "cart_id" => $cartId,
                        "product_id" => $product,
                        "quantity" => 1
                    ]
                );
            }
        }
        return true;
    }

    public static function getProductFromCart(int $cartId, int $productId): array
    {
        if (empty($cartId) || empty($productId)) {
            return [];
        }
        $stmt = Db::getInstance()->pdo->prepare(
            "
        SELECT
            *
        FROM
            `cart_products`
        WHERE
            `cart_id` = :cart_id
        AND `product_id` = :product_id
    "
        );
        $stmt->execute(
            [
                "cart_id" => $cartId,
                "product_id" => $productId
            ]
        );
        $prodArray =$stmt->fetchAll();
        $cartProduct = [];
        foreach ($prodArray as $cart) {
            $product = new CartProduct();
            $product->id = $cart['cart_id'];
            $product->category_id = $cart['product_id'];
            $product->title = $cart['quantity'];
            $cartProduct[] = $product;
        }
        return $cartProduct;
    }

    public static function getProductsFromCart(string $cartId): array
    {
        if (empty($cartId)) {
            return [];
        }
        $stmt = Db::getInstance()->pdo->prepare(
            "
        SELECT
            `products`.*,
            `cart_products`.`quantity` as selected_quantity
        FROM
            `cart_products`
        INNER JOIN `products` ON `products`.`id` = `cart_products`.`product_id`
        WHERE
            `cart_id` = :cart_id
    "
        );
        $stmt->execute(
            [
                "cart_id" => $cartId,
            ]
        );
        $prodsArray = $stmt->fetchAll();
        $cartProducts = [];
        foreach ($prodsArray as $cart) {
            $product = new CartProduct();
            $product->id = $cart['id'];
            $product->category_id = $cart['category_id'];
            $product->title = $cart['title'];
            $product->description = $cart['description'];
            $product->price = $cart['price'];
            $product->qty = $cart['qty'];
            $cartProducts[] = $product;
        }
        return $cartProducts;
    }

}